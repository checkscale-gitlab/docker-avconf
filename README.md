# docker-avconf

An avconf docker build to easily support live streaming from a given video device. Supports Raspberry Pis!

### Usage and FAQ
clone & build
```
git clone git@gitlab.com:adrift/docker-avconf.git
docker build . -t quay.io/adrift/docker-avconv
```

run
```
# optionally, provide extra -e options to this to override what is present in .env
docker run -it --env-file .env --device "/dev/video0:/dev/video0" quay.io/adrift/docker-avconv
```

##### Super custom options
Skip the compose file, and simply continue amending arguments to the `avconf` binary by supplying COMMANDs to the Docker run file. Review docker run documentation on exactly how to provide this at runtime.

##### I want to use more than one video device at runtime to do some wild AV magic
Not supported yet! Accepting PRs :^)
